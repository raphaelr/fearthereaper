﻿using System;
using System.IO.Pipes;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FearTheReaper
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var engine = new Engine();

            await using var server = new NamedPipeServerStream("\\\\.\\net.tapesoftware.fear-the-reaper",
                PipeDirection.In,
                NamedPipeServerStream.MaxAllowedServerInstances, PipeTransmissionMode.Message,
                PipeOptions.Asynchronous);

            Console.WriteLine("Opened named pipe");
            var buffer = new byte[512];
            while (true)
            {
                await server.WaitForConnectionAsync();

                try
                {
                    await ProcessClient(server, buffer, engine);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Server error: Cannot process client: " + ex);
                }

                server.Disconnect();
            }
        }

        private static async Task ProcessClient(NamedPipeServerStream stream, byte[] buffer, Engine engine)
        {
            var command = await GetCommand(stream, buffer);
            if (command.StartsWith("attach="))
            {
                var expectedParent = command.Substring(7);
                if (UnsafeNativeMethods.GetNamedPipeClientProcessId(stream.SafePipeHandle, out var childPid))
                {
                    await engine.Attach(childPid, expectedParent);
                }
            }
            else
            {
                Console.WriteLine("Unknown command: " + command);
            }
        }

        private static async Task<string> GetCommand(NamedPipeServerStream stream, byte[] buffer)
        {
            var cts = new CancellationTokenSource();
            cts.CancelAfter(TimeSpan.FromSeconds(5));
            var read = await stream.ReadAsync(buffer, 0, buffer.Length, cts.Token);
            return Encoding.UTF8.GetString(buffer, 0, read);
        }
    }
}
