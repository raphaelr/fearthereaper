﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace FearTheReaper
{
    public class Engine
    {
        private readonly ChannelWriter<IEngineEvent> _writer;
        public Engine()
        {
            var channel = Channel.CreateUnbounded<IEngineEvent>(new UnboundedChannelOptions
            {
                SingleReader = true
            });
            _writer = channel.Writer;
            Task.Run(() => EventLoop(channel.Reader));
        }

        public async Task Attach(int childPid, string expectedParent)
        {
            Console.WriteLine($"Attaching child {childPid} to parent {expectedParent}");
            var childProcess = TryGetProcessById(childPid);
            if (childProcess == null)
            {
                Console.WriteLine($"Error: Child {childPid} not found");
                return;
            }

            var currentHandle = childProcess.SafeHandle;
            while (true)
            {
                var basicInfo = new UnsafeNativeMethods.ProcessBasicInformation();
                var status = UnsafeNativeMethods.NtQueryInformationProcess(currentHandle.DangerousGetHandle(), 0, ref basicInfo,
                    Marshal.SizeOf(basicInfo), out _);
                if (status != 0)
                {
                    Console.WriteLine("Error: NtQueryInformationProcess returned " + status);
                    childProcess.Dispose();
                    return;
                }

                var parentPid = basicInfo.InheritedFromUniqueProcessId.ToInt32();
                var parentProcess = TryGetProcessById(parentPid);
                if (parentProcess == null)
                {
                    Console.WriteLine($"Error: Parent {parentPid} not found");
                    childProcess.Dispose();
                    return;
                }

                if (parentProcess.HasExited)
                {
                    Console.WriteLine("Error: Parent process exited, no whitelisted parent found");
                    childProcess.Dispose();
                    return;
                }

                if (parentProcess.ProcessName == expectedParent)
                {
                    await AttachIntern(childProcess, parentProcess);
                    return;
                }

                if (currentHandle != childProcess.SafeHandle)
                {
                    currentHandle.Dispose();
                }

                currentHandle = parentProcess.SafeHandle;
            }
        }

        private async Task AttachIntern(Process childProcess, Process parentProcess)
        {
            var childDesc = $"[{childProcess.Id} {childProcess.ProcessName}]";
            var parentDesc = $"[{parentProcess.Id} {parentProcess.ProcessName}]";

            var childWrapper = new ProcessWrapper(childProcess, childDesc);
            var parentWrapper = new ProcessWrapper(parentProcess, parentDesc);
            await _writer.WriteAsync(new RegisterEvent(childWrapper, parentWrapper));

            parentProcess.Exited += (sender, args) => { _writer.WriteAsync(new ExitEvent(parentWrapper)); };
            parentProcess.EnableRaisingEvents = true;

            childProcess.Exited += (sender, args) => { _writer.WriteAsync(new ExitEvent(childWrapper)); };
            childProcess.EnableRaisingEvents = true;
        }

        private async Task EventLoop(ChannelReader<IEngineEvent> reader)
        {
            var registrations = new List<RegisterEvent>();

            await foreach (var ev in reader.ReadAllAsync())
            {
                if (ev is RegisterEvent registerEvent)
                {
                    Console.WriteLine($"Schedule kill of {registerEvent.Child.DisplayName} if {registerEvent.Parent.DisplayName} exits");
                    registrations.Add(registerEvent);
                }
                else if (ev is ExitEvent exitEvent)
                {
                    Console.WriteLine("Received exit event for " + exitEvent.Process.DisplayName);
                    
                    var registration = registrations.SingleOrDefault(r => r.Child == exitEvent.Process || r.Parent == exitEvent.Process);
                    if (registration == null)
                    {
                        Console.WriteLine("Exit already handled, doing nothing");
                        continue;
                    }

                    if (exitEvent.Process == registration.Child)
                    {
                        Console.WriteLine("Child committed sudoku, detaching");
                    }
                    else
                    {
                        Console.WriteLine($"Parent {exitEvent.Process.DisplayName} exited, killing child");
                        try
                        {
                            registration.Child.Process.Kill(true);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Error: Could not kill child: " + ex);
                        }
                    }

                    registration.Child.Process.Dispose();
                    registration.Parent.Process.Dispose();
                    registrations.Remove(registration);
                }
            }
        }

        private static Process TryGetProcessById(int pid)
        {
            try
            {
                return Process.GetProcessById(pid);
            }
            catch (ArgumentException)
            {
                return null;
            }
        }

        private class ProcessWrapper
        {
            public Process Process { get; }
            public string DisplayName { get; }

            public ProcessWrapper(Process process, string displayName)
            {
                Process = process;
                DisplayName = displayName;
            }
        }

        private interface IEngineEvent { }

        private class RegisterEvent : IEngineEvent
        {
            public ProcessWrapper Child { get; }
            public ProcessWrapper Parent { get; }

            public RegisterEvent(ProcessWrapper child, ProcessWrapper parent)
            {
                Child = child;
                Parent = parent;
            }
        }

        private class ExitEvent : IEngineEvent
        {
            public ProcessWrapper Process { get; }

            public ExitEvent(ProcessWrapper process)
            {
                Process = process;
            }
        }
    }
}